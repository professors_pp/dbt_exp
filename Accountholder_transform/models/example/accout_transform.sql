{{ config(
    materialized="table"
) }}

with accountmanagers as (
    select
        _airbyte_emitted_at,
        (current_timestamp at time zone 'utc')::timestamp as _airbyte_normalized_at,

        cast(jsonb_extract_path_text("_airbyte_data",'Email') as varchar) as "Email",
        cast(jsonb_extract_path_text("_airbyte_data",'Mobile') as varchar) as "Mobile",
        cast(jsonb_extract_path_text("_airbyte_data",'LastName') as varchar) as LastName,
        cast(jsonb_extract_path_text("_airbyte_data",'FirstName') as varchar) as FirstName,
        cast(jsonb_extract_path_text("_airbyte_data",'EntityTimestamp') as varchar) as EntityTimestamp,
        cast(jsonb_extract_path_text("_airbyte_data",'Version') as varchar) as new_confirmed,
        cast(jsonb_extract_path_text("_airbyte_data",'IsActive') as boolean) as IsActive,
        cast(jsonb_extract_path_text("_airbyte_data",'AccountManagerId') as varchar) as AccountManagerId,
        cast(jsonb_extract_path_text("_airbyte_data",'_ab_source_file_url') as varchar) as file_source_path
    from "Airflow_testDb".staging._airbyte_raw_srcaccountmanagers
)
select * from accountmanagers